## Copper

The source ontology diagrams are split up by concepts:

- [Measurements](https://app.diagrams.net/#Akupferdigital%2Fontologies%2Fcopper%2Fmaster%2Fmeasurements.drawio)
- [Acts](https://app.diagrams.net/#Akupferdigital%2Fontologies%2Fcopper%2Fmaster%2Facts.drawio)
- [Materials](https://app.diagrams.net/#Akupferdigital%2Fontologies%2Fcopper%2Fmaster%2Fmaterials.drawio)
- [Standards](https://app.diagrams.net/#Akupferdigital%2Fontologies%2Fcopper%2Fmaster%Fstandards.drawio)
